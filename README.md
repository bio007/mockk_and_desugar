# README #

This project demonstrates the issue with [mockk](https://mockk.io/) library in combination with Android's [java 8 support](https://developer.android.com/studio/write/java8-support) via desugaring.

Project contains example instrumentation test utilizing simple mockk which upon running in release configuration crashes with
```java
java.lang.NoSuchMethodError: No direct method <init>(IF)V in class Lj$/util/concurrent/ConcurrentHashMap; or its super classes (declaration of 'j$.util.concurrent.ConcurrentHashMap' appears in /data/app/.../base.apk!classesX.dex)
        at com.android.dx.rop.type.Type.<clinit>(Type.java:35)
```

Release configuration is important in this casse as **the stripping of the APK** will efectivelly remove seemingly unused code mockk is relying on to. Debuggable APK is not stripped down so the issue does not occure.

### Running test in release configuration
`./gradlew :test:connectedReleaseAndroidTest -PtestBuildType=release`