package com.example.test

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.

        val mock = mockk<Car>()
        every { mock.engine } returns "3.0"

        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.example.test.test", appContext.packageName)
    }

    private class Car {
        val engine: String = "2.0"
    }
}